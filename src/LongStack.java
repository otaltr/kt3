import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class LongStack {

   public static void main (String[] argum) {
      System.out.println(interpret("6 7 SWAP + -"));
   }

   private LinkedList<Long> list = new LinkedList<>();

   LongStack() {}

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack copyOfStack = new LongStack();
      copyOfStack.list.addAll(list);
      return copyOfStack;
   }

   public boolean stEmpty() {
      return list.size() == 0;
   }

   public void push (long a) {
      list.add(a);
   }

   public long pop() {

      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Too few elements in the expression");
      }

      long elementToPop = list.getLast();
      list.remove(list.size() - 1);

      return elementToPop;
   }

   public void op (String s) {

      if (list.size() < 2)
         throw new IndexOutOfBoundsException("Too few elements in the expression");
      if (list.size() < 3 && s.equals("ROT"))
         throw new IndexOutOfBoundsException("Too few elements in the expression");

      long op2 = pop();
      long op1 = pop();

      ArrayList<String> validOperators = new ArrayList<>(Arrays.asList("+", "-", "*", "/", "SWAP", "ROT"));

      if (validOperators.contains(s)) {
         if (s.equals("+"))
            push(op1 + op2);
         else if (s.equals("-"))
            push(op1 - op2);
         else if (s.equals("*"))
            push(op1 * op2);
         else if (s.equals("/"))
            push(op1 / op2);
         else if (s.equals("SWAP")){
            push(op2);
            push(op1);
            }
         else if (s.equals("ROT")) {
            long op3 = pop();
            push(op1);
            push(op2);
            push(op3);
         }
      }
      else {
         throw new IllegalArgumentException("Invalid symbol");
      }
   }
  
   public long tos() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Too few elements in the expression");
      }
      return list.getLast();
   }

   public long get(int index) {
      return list.get(index);
   }

   @Override
   public boolean equals (Object o) {
      if (((LongStack) o).list.size() != list.size())
         return false;
      for (int i = 0; i < list.size(); i++) {
         if (((LongStack) o).get(i) != list.get(i)) {
            return false;
         }
      }
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty())
         return "empty";
      StringBuilder str = new StringBuilder();

      for (Long element : list) {
         str.append(element.toString()).append(" ");
      }
      return str.toString();
   }

   public static long interpret (String pol) {

      String[] splitString = pol.trim().replace("\t", " ").split(" ");

      if (pol.equals("")) {
         throw new RuntimeException("Empty expression");
      }

      try {
         if (splitString.length > 1) {
            Long.parseLong(splitString[splitString.length - 1]);
            throw new RuntimeException("Too many numbers in expression: " + "'" + pol + "'");
         }
      }
      catch (NumberFormatException ignored)
      {
      }

      LongStack myStack = getLongStack(pol, splitString);

      return myStack.tos();
   }

   private static LongStack getLongStack(String pol, String[] splitString) {

      LongStack myStack = new LongStack();

      for (String element : splitString) {

         if (element.equals("")) {
            continue;
         }

         try {
            myStack.push(Long.parseLong(element)); // IF ELEMENT HAPPENS TO BE AN INTEGER
         } catch (NumberFormatException e) // IF ELEMENT IS NOT AN INTEGER
         {
            try {
               myStack.op(element);
            } catch (IllegalArgumentException f) {
               throw new IllegalArgumentException("Invalid symbol " + element + " in expression " + "'" + pol + "'");
            } catch (IndexOutOfBoundsException g) {
               throw new RuntimeException("Cannot perform " + element + " in expression " + "'" + pol + "'");
            }
         }


         }
      return myStack;
      }


}

